<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', 'HomeController@index')->name('home');

//Establece un nuevo rol a un usuario, utilizando la funcion definida en HomeController setUserRole()
Route::get('/setRole', 'HomeController@setUserRole')->name('setUserRole');

Auth::routes();
