@extends('layouts.app')
//Pagina de inicio de los usuarios con permiso "super"
@section('content')
<div class="container pt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="background-color: #fff;
                border: 2px solid #000;
                box-shadow: 7px 7px 0 0 #000;
                color:  #000;
                font-family: 'PT Sans', sans-serif;">
                <div class="card-header" style="font-family: 'Ubuntu Condensed', sans-serif;">Laravel Login</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Success! You are now logged in.
                    SUPER
                </div>

                <div>
                    <table class="table table-responsive" id="states-table">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th colspan="3">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                            //Cuadro de usuarios con opción para establecer sus roles. 
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                //Pasa el parametro del id y rol a la funcion setUserRole() establecida en el HomeController, a traves de las ruta.
                                <td>
                                    <a href="{{ route('setUserRole',['user' => $user->id,'role' => 'user']) }}"
                                       type="button" class="btn btn-default">User</a>
                                    <a href="{{ route('setUserRole',['user' => $user->id,'role' => 'admin']) }}"
                                       type="button" class="btn btn-default">Admin</a>
                                    <a href="{{ route('setUserRole',['user' => $user->id,'role' => 'super']) }}"
                                       type="button" class="btn btn-default">Super</a>
                                </td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
