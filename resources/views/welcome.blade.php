<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Ingeniería de Software</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=PT+Sans|Ubuntu+Condensed&display=swap" rel="stylesheet"> 

        <!-- Styles -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <style>
            html, body {
                background-color: #e3f2fd;
                color: #636b6f;
                font-family: 'PT Sans', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                background: url(/img/background.png);
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 65px;
                font-family: 'Ubuntu Condensed', sans-serif;
            }

            .links > a {
                color: black;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
                font-family: 'Ubuntu Condensed', sans-serif;
            }

            .m-b-md {
                margin-bottom: 10px;
            }
            .heading{
                background-color: #fff;
                border: 2px solid #000;
                box-shadow: 7px 7px 0 0 #000;
                color:  #000;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <div class="flex-center position-ref full-height">
                //Redirige al login/register o home
                    @if (Route::has('login'))
                        <div class="top-right links heading">
                            @auth
                                <a href="{{ url('/home') }}">Home</a>
                            @else
                                <a href="{{ route('login') }}" >Login</a>
        
                                @if (Route::has('register'))
                                    <a href="{{ route('register') }}">Register</a>
                                @endif
                            @endauth
                        </div>
                    @endif
        
                    <div class="content">
                        <div class="heading m-b-md">
                            <div class="title">
                                            Ingeniería de Software
                                        </div>
                                        <div>
                                            <p>Integrantes:</p>
                                            <p style="font-size: 13px;">Víctor Madera, Leonardo Galeano, Adriana Rodríguez, Andrea Godoy, Alfonso Briceño</p>
                                        </div>
                                    </div>
        
                        <div class="links">
                            <a href="https://laravel.com/docs">Docs</a>
                            <a href="https://laracasts.com">Laracasts</a>
                            <a href="https://laravel-news.com">News</a>
                            <a href="https://blog.laravel.com">Blog</a>
                            <a href="https://nova.laravel.com">Nova</a>
                            <a href="https://forge.laravel.com">Forge</a>
                            <a href="https://vapor.laravel.com">Vapor</a>
                            <a href="https://github.com/laravel/laravel">GitHub</a>
                        </div>
                    </div>
                </div></div>
    </body>
</html>
