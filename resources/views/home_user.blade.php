@extends('layouts.app')
//Pagina de inicio del usuario con permisos "user"
@section('content')
<div class="container pt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="background-color: #fff;
                border: 2px solid #000;
                box-shadow: 7px 7px 0 0 #000;
                color:  #000;
                font-family: 'PT Sans', sans-serif;">
                <div class="card-header" style="font-family: 'Ubuntu Condensed', sans-serif;">Laravel Login</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Success! You are now logged in.
                    USER
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
