<?php

use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class FillPermissionTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permissions = [
            'home_super',
            'home_admin',
            'home_user'
        ];

        $roles = [
            'super',
            'admin',
            'user'
        ];

        $role = Role::create(['name' => 'super']);
        foreach ($permissions as $permission){
            $perm = Permission::create(['name' => $permission]);
            $role->givePermissionTo($perm);
        }

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo('home_admin');

        $role = Role::create(['name' => 'user']);
        $role->givePermissionTo('home_user');

        $user = User::create([
            'name' => 'Andrea Godoy',
            'email' => 'carmen.godoy@alumnos.uneatlantico.es',
            'password' => Hash::make('12345678'),
        ]);
        $user->assignRole('super');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('SET FOREIGN_KEY_CHECKS = 0;');
        DB::unprepared('TRUNCATE permissions;');
        DB::unprepared('TRUNCATE model_has_permissions;');
        DB::unprepared('TRUNCATE model_has_roles;');
        DB::unprepared('TRUNCATE roles;');
        DB::unprepared('TRUNCATE role_has_permissions;');
        DB::unprepared('SET FOREIGN_KEY_CHECKS = 1; ');
    }
}
