<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Se establecen permisos para cada tipo de usuario.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:home_super', ['only' => ['indexSuper', 'setUserRole']]);
        $this->middleware('permission:home_admin', ['only' => ['indexAdmin']]);
        $this->middleware('permission:home_user', ['only' => ['indexUser']]);
    }

    /**
     * Se enseña la plantilla de la aplicación correspondiendo a los permisos que tenga ese usuario.
     *
     * @return Renderable
     */
    public function index()
    {
        $user = Auth::user();
        if($user->can('home_super')){
            return $this->indexSuper();
        } else if($user->can('home_admin')){
            return $this->indexAdmin();
        }else if($user->can('home_user')){
            return $this->indexUser();
        } else{
            return view('home');
        }


    }

//Redirige al inicio de los usuarios con permiso de "User"
    public function indexUser()
    {
        return view('home_user');
    }
//Redirige al inicio de admin a los usuarios con permiso de "admin"
    public function indexAdmin()
    {
        return view('home_admin');
    }

//Redirige al inicio de super a los usuarios con permiso de "super"
    public function indexSuper()
    {
        $users = User::all();
        return view('home_super')->with('users', $users);
    }
    
    //Permite definir un nuevo rol a un tipo de usuario, el cual es un permiso de los usuarios "Super"

    public function setUserRole(Request $request)
    {
        if ($request->user != 1) {
            $user = User::find($request->user);
            $user->syncRoles($request->role);
        }
        return redirect(route('home'));
    }






}
